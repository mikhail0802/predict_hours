import pandas as pd
from sklearn.linear_model import LogisticRegression

def get_weather():
    weather1920 = pd.read_html('weather1920.html')
    weather1920 = weather1920[0][['ÐÐ°ÑÐ°', 'Ð¡ÑÐµÐ´Ð½ÑÑ ÑÐµÐ¼Ð¿ÐµÑÐ°ÑÑÑÐ°',\
    'Ð¡ÐºÐ¾ÑÐ¾ÑÑÑ Ð²ÐµÑÑÐ°']]
    weather1920.rename(columns = {'ÐÐ°ÑÐ°' : 'day', 'Ð¡ÑÐµÐ´Ð½ÑÑ ÑÐµÐ¼Ð¿ÐµÑÐ°ÑÑÑÐ°' : 'tmp',\
    'Ð¡ÐºÐ¾ÑÐ¾ÑÑÑ Ð²ÐµÑÑÐ°' : 'wind'}, inplace = True)
    
    jan_okt = []
    jan2021 = pd.read_html('jan2021.html')
    jan_okt.append(jan2021[0].iloc[0:len(jan2021[0]) - 1, [0, 1, 5, 6]])
    feb2021 = pd.read_html('feb2021.html')
    jan_okt.append(feb2021[0].iloc[0:len(feb2021[0]) - 1, [0, 1, 5, 6]])
    march2021 = pd.read_html('march2021.html')
    jan_okt.append(march2021[0].iloc[0:len(march2021[0]) - 1, [0, 1, 5, 6]])
    apr2021 = pd.read_html('apr2021.html')
    jan_okt.append(apr2021[0].iloc[0:len(apr2021[0]) - 1, [0, 1, 5, 6]])
    may2021 = pd.read_html('may2021.html')
    jan_okt.append(may2021[0].iloc[0:len(may2021[0]) - 1, [0, 1, 5, 6]])
    june2021 = pd.read_html('june2021.html')
    jan_okt.append(june2021[0].iloc[0:len(june2021[0]) - 1, [0, 1, 5, 6]])
    july2021 = pd.read_html('july2021.html')
    jan_okt.append(july2021[0].iloc[0:len(july2021[0]) - 1, [0, 1, 5, 6]])
    aug2021 = pd.read_html('aug2021.html')
    jan_okt.append(aug2021[0].iloc[0:len(aug2021[0]) - 1, [0, 1, 5, 6]])
    sep2021 = pd.read_html('sep2021.html')
    jan_okt.append(sep2021[0].iloc[0:len(sep2021[0]) - 1, [0, 1, 5, 6]])
    okt2021 = pd.read_html('okt2021.html')
    jan_okt.append(okt2021[0].iloc[0:len(okt2021[0]) - 1, [0, 1, 5, 6]])
    
    jan_okt[9].iloc[20, 3] = 7
    jan_okt[9].loc[30] = jan_okt[9].loc[27]; jan_okt[9].loc[29] = jan_okt[9].loc[28]
    for i in range(6):
        jan_okt[9].loc[28 - i] = jan_okt[9].loc[26 - i]
    jan_okt[9].loc[21] = [22, +10, 7, +10]; jan_okt[9].loc[22] = [23, +6, 5, +4]
    
    for i in range(len(jan_okt)):
        day = []
        for j in range(len(jan_okt[i])):
            day_vsp = str()
            if j + 1 < 10:
                day_vsp += '0'
            day_vsp += str(jan_okt[i].iloc[j, 0]) + '.'
            if i + 1 < 10:
                day_vsp += '0'
            day_vsp += str(i + 1) + '.' + '2021'
            day.append(day_vsp)
        tmp = []
        for j in range(len(jan_okt[i])):
            tmp.append((float(jan_okt[i].iloc[j, 1]) + float(jan_okt[i].iloc[j, 3]))/2)
        wind = []
        for j in range(len(jan_okt[i])):
            wind_vsp = str()
            if (jan_okt[i].iloc[j, 2] == 'Ð¨') | (jan_okt[i].iloc[j, 2] == 'NaN'):
                wind_vsp = '0'
            else:
                for k in range(len(str(jan_okt[i].iloc[j, 2]))):
                    if (str(jan_okt[i].iloc[j, 2])[k] == '0') | (str(jan_okt[i].iloc[j, 2])[k] == '1') | \
                    (str(jan_okt[i].iloc[j, 2])[k] == '2') | (str(jan_okt[i].iloc[j, 2])[k] == '3') | \
                    (str(jan_okt[i].iloc[j, 2])[k] == '4') | (str(jan_okt[i].iloc[j, 2])[k] == '5') | \
                    (str(jan_okt[i].iloc[j, 2])[k] == '6') | (str(jan_okt[i].iloc[j, 2])[k] == '7') | \
                    (str(jan_okt[i].iloc[j, 2])[k] == '8') | (str(jan_okt[i].iloc[j, 2])[k] == '9') | \
                    (str(jan_okt[i].iloc[j, 2])[k] == '.'):
                        wind_vsp += str(jan_okt[i].iloc[j, 2])[k]
            wind.append(wind_vsp)
        d = {'day' : day, 'tmp' : tmp, 'wind' : wind}
        jan_okt[i] = pd.DataFrame(data = d)
        
    weather = weather1920
    for i in range(len(jan_okt)):
        weather = weather.append(jan_okt[i])
        
    return weather
    

def get_oil():
    oil = pd.read_excel('нефть-brent (2).xlsx')
    oil.rename(columns = {'Дата' : 'day', 'Значение' : 'oil_price'}, inplace = True)
    oil['daystr'] = str()

    for i in range(len(oil)):
        day_vsp = str()
        if oil['day'].iloc[i].day < 10:
            day_vsp += '0'
        day_vsp += str(oil['day'].iloc[i].day) + '.'
        if oil['day'].iloc[i].month < 10:
            day_vsp += '0'
        day_vsp += str(oil['day'].iloc[i].month) + '.' + str(oil['day'].iloc[i].year)
        oil['daystr'].iloc[i] = day_vsp
        
    return oil


def get_hour():
    chas19 = pd.read_excel('prices_psk_2019.xlsx', sheet_name = 'Часы региона')
    chas20 = pd.read_excel('prices_psk_2020.xlsx', sheet_name = 'Часы региона')
    chas21 = pd.read_excel('prices_2021.xlsx', sheet_name = 'Часы региона')
    chas = chas19.append(chas20).append(chas21)
    chas['daystr'] = str()
    
    for i in range(31):
        if (pd.Timestamp('2021-10-' + str(i + 1)).weekday() >= 0) & (pd.Timestamp('2021-10-' + str(i + 1)).weekday() <= 4):
            chas.loc[len(chas)] = [pd.Timestamp('2021-10-' + str(i + 1)), 0, 0]

    for i in range(len(chas)):
        day_vsp = str()
        if chas['day'].iloc[i].day < 10:
            day_vsp += '0'
        day_vsp += str(chas['day'].iloc[i].day) + '.'
        if chas['day'].iloc[i].month < 10:
            day_vsp += '0'
        day_vsp += str(chas['day'].iloc[i].month) + '.' + str(chas['day'].iloc[i].year)
        chas['daystr'].iloc[i] = day_vsp
        
    return chas
    

def get_new_base():
    chas = get_hour(); weather = get_weather(); oil = get_oil()
    chas['tmp'] = 0; chas['wind'] = 0
    for i in range(len(chas)):
        for j in range(len(weather)):
            if chas['daystr'].iloc[i] == weather['day'].iloc[j]:
                chas['tmp'].iloc[i] = weather['tmp'].iloc[j]
                chas['wind'].iloc[i] = weather['wind'].iloc[j]
                break
                
    chas['oil_price'] = 0
    for i in range(len(chas)):
        flag = False
        for j in range(len(oil)):
            if chas['daystr'].iloc[i] == oil['daystr'].iloc[j]:
                chas['oil_price'].iloc[i] = oil['oil_price'].iloc[j]
                flag = True
                break
        if flag == False:
            chas['oil_price'].iloc[i] = chas['oil_price'].iloc[i - 1]

    year = chas['day'].dt.year
    day = chas['day'].dt.day
    month = chas['day'].dt.month
    weekday = chas['day'].dt.weekday
    tmp = chas['tmp']
    wind = chas['wind']
    oil_price = chas['oil_price']
    hour = chas['hour']
    d = {'year' : year, 'day' : day, 'month' : month, 'weekday' : weekday,
         'tmp' : tmp, 'wind' : wind, 'oil_price' : oil_price, 'hour' : hour}
    data = pd.DataFrame(data = d)
    
    return data
    

def predict_values(month):
    data = get_new_base()
    X_train1 = data[(data['year'] == 2019) & (data['month'] == month - 1) & (data['day'] > 25) | \
                (data['year'] == 2019) & (data['month'] == month) & (data['day'] <= 15) | \
                (data['year'] == 2020) & (data['month'] == month - 1) & (data['day'] > 25) | \
                (data['year'] == 2020) & (data['month'] == month) & (data['day'] <= 15) | \
                (data['year'] == 2021) & (data['month'] == month - 1) & (data['day'] > 25)].\
    drop({'hour', 'day'}, axis = 1)
    y_train1 = data[(data['year'] == 2019) & (data['month'] == month - 1) & (data['day'] > 25) | \
                (data['year'] == 2019) & (data['month'] == month) & (data['day'] <= 15) | \
                (data['year'] == 2020) & (data['month'] == month - 1) & (data['day'] > 25) | \
                (data['year'] == 2020) & (data['month'] == month) & (data['day'] <= 15) | \
                (data['year'] == 2021) & (data['month'] == month - 1) & (data['day'] > 25)]['hour']
                
    X_train2 = data[(data['year'] == 2019) & (data['month'] == month) & (data['day'] >= 15) | \
                (data['year'] == 2019) & (data['month'] == month + 1) & (data['day'] < 5) | \
                (data['year'] == 2020) & (data['month'] == month) & (data['day'] >= 15) | \
                (data['year'] == 2020) & (data['month'] == month + 1) & (data['day'] < 5)].\
    drop({'hour', 'day'}, axis = 1)
    y_train2 = data[(data['year'] == 2019) & (data['month'] == month) & (data['day'] >= 15) | \
                (data['year'] == 2019) & (data['month'] == month + 1) & (data['day'] < 5) | \
                (data['year'] == 2020) & (data['month'] == month) & (data['day'] >= 15) | \
                (data['year'] == 2020) & (data['month'] == month + 1) & (data['day'] < 5)]['hour']
                
    X_test1 = data[(data['year'] == 2021) & (data['month'] == month) & (data['day'] <= 15)].drop({'hour', 'day'}, axis = 1)
    X_test2 = data[(data['year'] == 2021) & (data['month'] == month) & (data['day'] > 15)].drop({'hour', 'day'}, axis = 1)
    
    X_vsp1 = data[(data['year'] == 2021) & (data['month'] == month) & (data['day'] <= 15)]
    X_vsp2 = data[(data['year'] == 2021) & (data['month'] == month) & (data['day'] > 15)]
    
    new_classifier1 = LogisticRegression(max_iter = 10000)
    new_classifier1.fit(X_train1, y_train1)

    new_classifier2 = LogisticRegression(max_iter = 10000)
    new_classifier2.fit(X_train2, y_train2)

    y_predict1 = new_classifier1.predict(X_test1)
    y_predict_proba1 = new_classifier1.predict_proba(X_test1)

    y_predict2 = new_classifier2.predict(X_test2)
    y_predict_proba2 = new_classifier2.predict_proba(X_test2)
    
    day = []; hour = []; proba = []
    d = {'day' : day, 'hour' : hour, 'proba' : proba}
    new_data = pd.DataFrame(data = d)
    
    for i in range(len(y_predict1)):
        #print('')
        #print(X_vsp1['day'].iloc[i])
        for j in range(len(y_predict_proba1[i])):
            if y_predict_proba1[i][j] > 0.05:
                #print(new_classifier1.classes_[j], ': ', round(y_predict_proba1[i][j]*10000)/100, '%')
                new_data.loc[len(new_data)] = [X_vsp1['day'].iloc[i], new_classifier1.classes_[j],\
                round(y_predict_proba1[i][j]*10000)/100]
                
    for i in range(len(y_predict2)):
        #print('')
        #print(X_vsp2['day'].iloc[i])
        for j in range(len(y_predict_proba2[i])):
            if y_predict_proba2[i][j] > 0.05:
                #print(new_classifier2.classes_[j], ': ', round(y_predict_proba2[i][j]*10000)/100, '%')
                new_data.loc[len(new_data)] = [X_vsp2['day'].iloc[i], new_classifier2.classes_[j],\
                round(y_predict_proba2[i][j]*10000)/100]
    
    new_data.to_excel('./proba.xlsx', index = False)
    
    return new_data


def get_true_values(month):
    data = get_new_base()
    true_values = data[(data['year'] == 2021) & (data['month'] == month)].drop({'year', 'month', 'weekday',\
    'tmp', 'wind', 'oil_price'}, axis = 1)
    return true_values